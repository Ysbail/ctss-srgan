from dataclasses import dataclass

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.use("Agg")

from src.losses import (
    mae_loss,
    mse_loss,
    its_a_fake_loss_func,
    true_jedi_loss_func,
    psnr_loss_func,
)


class Trainer:
    def __init__(self, gen_model, dis_model, den_model, **kwargs):
        self.gen_model = gen_model
        self.dis_model = dis_model
        self.den_model = den_model
    
        self.gen_opt = kwargs.get('gen_opt', tf.keras.optimizers.Adam(1e-4))
        self.dis_opt = kwargs.get('dis_opt', tf.keras.optimizers.Adam(1e-4))
        self.den_opt = kwargs.get('den_opt', tf.keras.optimizers.Adam(1e-3))

        self.gen_tracker = tf.keras.metrics.Mean(name='gen_loss')
        self.dis_tracker = tf.keras.metrics.Mean(name='dis_loss')
        self.den_tracker = tf.keras.metrics.Mean(name='den_loss')
        
        self.gen_model = _Model(gen_model, self.gen_opt, self.gen_tracker)
        self.dis_model = _Model(dis_model, self.dis_opt, self.dis_tracker)
        self.den_model = _Model(den_model, self.den_opt, self.den_tracker)
        
    '''
    def _train_model(self, tape, opt, loss, model_vars, tracker):
        _gradient = tape.gradient(loss, model_vars)
        opt.apply_gradients(zip(_gradient, model_vars))
        tracker.update_state(loss)
    '''
    
    
    def use_pre_trained_weights(self, _model, _path):
        getattr(self, _model).load_weights(_path)
        
    
    def train_step_den(self, lr, hr):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as dis_tape, tf.GradientTape() as den_tape:
            sr = self.gen_model(lr, training=True)
            sr_den = self.den_model(sr, training=True)
            
            hr_predict = self.dis_model(hr, training=True)
            sr_predict = self.dis_model(sr, training=True)
            sr_den_predict = self.dis_model(sr_den, training=True)
            
            gen_sr_loss = mse_loss(hr, sr)
            gen_psnr_loss = -psnr_loss_func(hr, sr)
            
            den_sr_loss = mse_loss(hr, sr_den)
            den_psnr_loss = -psnr_loss_func(hr, sr_den)
            
            gen_loss = 1e-3 * true_jedi_loss_func(sr_predict) + gen_psnr_loss + gen_sr_loss # seeks how to deceive the discriminator
            den_loss = 1e-3 * true_jedi_loss_func(sr_den_predict) + den_psnr_loss + den_sr_loss
            dis_loss = its_a_fake_loss_func(sr_predict) + its_a_fake_loss_func(sr_den_predict) + true_jedi_loss_func(hr_predict)
            
        self.gen_model.optimize(gen_tape, gen_loss)
        self.dis_model.optimize(dis_tape, dis_loss)
        self.den_model.optimize(den_tape, den_loss)
        
    
    def train_step(self, lr, hr):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as dis_tape:
            sr = self.gen_model(lr, training=True)
            
            hr_predict = self.dis_model(hr, training=True)
            sr_predict = self.dis_model(sr, training=True)
            
            gen_sr_loss = mse_loss(hr, sr)
            gen_psnr_loss = -psnr_loss_func(hr, sr)
            
            gen_loss = 1e-3 * true_jedi_loss_func(sr_predict) + gen_psnr_loss + gen_sr_loss # seeks how to deceive the discriminator
            dis_loss = its_a_fake_loss_func(sr_predict) + true_jedi_loss_func(hr_predict)
        
        self.gen_model.optimize(gen_tape, gen_loss)
        self.dis_model.optimize(dis_tape, dis_loss)
    
    
    def train_step_fake_hr(self, lr, hr):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as dis_tape:
            sr = self.gen_model(lr, training=True)
            
            hr_predict = self.dis_model(hr, training=True)
            sr_predict = self.dis_model(sr, training=True)
            
            gen_sr_loss = mse_loss(hr, sr)
            gen_psnr_loss = -psnr_loss_func(hr, sr)
            
            gen_loss = 1e-3 * true_jedi_loss_func(sr_predict) + gen_psnr_loss + gen_sr_loss # seeks how to deceive the discriminator
            dis_loss = its_a_fake_loss_func(sr_predict) + its_a_fake_loss_func(hr_predict)
        
        self.gen_model.optimize(gen_tape, gen_loss)
        self.dis_model.optimize(dis_tape, dis_loss)
        
    
    def train_step_ignore_hr(self, lr, hr):
        with tf.GradientTape() as gen_tape:
            sr = self.gen_model(lr, training=True)
            
            sr_predict = self.dis_model(sr, training=False)
            
            gen_sr_loss = mse_loss(hr, sr)
            gen_psnr_loss = -psnr_loss_func(hr, sr)
            
            gen_loss = 1e-3 * true_jedi_loss_func(sr_predict) + gen_psnr_loss + gen_sr_loss # seeks how to deceive the discriminator
        
        self.gen_model.optimize(gen_tape, gen_loss)
        
                
    def plot(self, dataset, idx, prefix='test'):
        _extras = 2
        _rows = dataset.x_test.shape[0]
        _cols = dataset.x_test.shape[-1] + _extras
        lr = dataset.x_test
        sr = self.gen_model.predict(dataset.x_test) 
        sr_den = self.den_model.predict(sr)
        hr = dataset.y_test
        
        fig = plt.figure(figsize=(7. * _cols, 7. * _rows))
        for i in range(_rows):
            for j in range(_cols - _extras):
                plt.subplot(_rows, _cols, i * _cols + j + 1)
                plt.pcolormesh(lr[i, ..., j], cmap='cividis', vmin=-1., vmax=1.)
                plt.colorbar()
                
            # extra cols
            plt.subplot(_rows, _cols, (i + 1) * _cols - (_extras - 1))
            plt.pcolormesh(hr[i, ..., 0], cmap='cividis', vmin=-1., vmax=1.)
            plt.colorbar()
            
            plt.subplot(_rows, _cols, (i + 1) * _cols - (_extras - 2))
            plt.pcolormesh(sr[i, ..., 0], cmap='cividis', vmin=-1., vmax=1.)
            plt.title('mse loss: %f'%mse_loss(hr[i], sr[i]).numpy() + '\npsnr loss: %f'%(-psnr_loss_func(hr[i], sr[i])))
            plt.colorbar()
            
            # plt.subplot(_rows, _cols, (i + 1) * _cols - (_extras - 3))
            # plt.pcolormesh(sr_den[i, ..., 0], cmap='cividis', vmin=-1., vmax=1.)
            # plt.colorbar()
            
            # plt.subplot(_rows, _cols, (i + 1) * _cols - (_extras - 4))
            # plt.pcolormesh(sr_den[i, ..., 0] - sr[i, ..., 0], cmap='bwr', vmin=-1., vmax=1.)
            # plt.colorbar()
            
        plt.savefig(f'./imgs/{prefix}_plot_batch_%02d.jpg'%(idx + 1))
        plt.close()
    
     
class _Model:
    def __init__(self, model, opt, tracker):
        self.model = model
        self.opt = opt
        self.tracker = tracker
        
        self.vars = self.model.trainable_variables
        
        
    def optimize(self, tape, loss):
        _grad = tape.gradient(loss, self.vars)
        self.opt.apply_gradients(zip(_grad, self.vars))
        self.tracker.update_state(loss)
    
    
    # ugly funcs but works
    def save(self, *args, **kwargs):
        self.model.save(*args, **kwargs)
    
    
    def load_weights(self, *args, **kwargs):
        self.model.load_weights(*args, **kwargs)
        
        
    def predict(self, *args, **kwargs):
        return self.model.predict(*args, **kwargs)
    
    
    def __call__(self, *args, **kwargs):
        return self.model(*args, **kwargs)
        
        
import tensorflow as tf
from tensorflow.keras.losses import (
    MeanSquaredError,
    MeanAbsoluteError,
    BinaryCrossentropy,
)

binary_loss = BinaryCrossentropy(from_logits=False)
mse_loss = MeanSquaredError()
mae_loss = MeanAbsoluteError()


def its_a_fake_loss_func(_fake):
    loss = binary_loss(tf.zeros_like(_fake), _fake)
    return loss


def true_jedi_loss_func(_true):
    loss = binary_loss(tf.ones_like(_true), _true)
    return loss


def psnr_loss_func(y_true, y_pred):
    y_true = tf.cast(y_true, 'float32')
    y_pred = tf.cast(y_pred, 'float32')
    return -10. * tf.math.log(mse_loss(y_pred, y_true)) / tf.math.log(tf.constant(10., 'float32'))
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
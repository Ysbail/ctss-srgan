# import os
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
import tensorflow.keras.backend as K

from tensorflow.keras.layers import (
    Add,
    ReLU,
    Dense,
    Input,
    PReLU,
    Conv2D,
    Lambda,
    Dropout,
    Flatten,
    Permute,
    Reshape,
    LeakyReLU,
    Activation,
    Concatenate,
    MaxPooling2D,
    RepeatVector,
    Conv2DTranspose,
    SeparableConv2D,
    TimeDistributed,
    BatchNormalization,
    )

from tensorflow.keras.losses import (
    MeanSquaredError,
    BinaryCrossentropy,
    )
    
from tensorflow.keras.models import (
    Model,
    )
    
from tensorflow.keras.initializers import (
    Constant,
    RandomNormal,
    )
    
from tensorflow.keras.optimizers import (
    Adam,
    )


def srgan_upsampling(_nn):
    # _nn = Lambda(lambda x: tf.pad(x, ((0, 0), (1, 1), (1, 1), (0, 0)), 'REFLECT'))(_nn) # added
    _nn = Conv2D(filters=256, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear', kernel_initializer=RandomNormal(0., 0.02))(_nn) # modified
    _nn = Lambda(lambda x: tf.nn.depth_to_space(x, block_size=2))(_nn)
    _nn = PReLU(alpha_initializer=Constant(0.25), shared_axes=(1, 2))(_nn)
    return _nn


def srgan_conv_bn(_nn, kernel_size=(3, 3)):
    # _nn = Lambda(lambda x: tf.pad(x, ((0, 0), (1, 1), (1, 1), (0, 0)), 'REFLECT'))(_nn) # added
    _nn = Conv2D(filters=64, kernel_size=kernel_size, strides=(1, 1), padding='same', activation='linear', kernel_initializer=RandomNormal(0., 0.02))(_nn) # modified
    _nn = BatchNormalization(gamma_initializer=RandomNormal(1., 0.02))(_nn)
    return _nn


def srgan_res_block(_n):
    _nn = srgan_conv_bn(_n)
    _nn = PReLU(alpha_initializer=Constant(0.25), shared_axes=(1, 2))(_nn)
    _nn = srgan_conv_bn(_nn)
    _nn = Add()([_n, _nn])
    return _nn
    
    
def srgan_generator_model(num_images, optimizer='adam', loss='mse'):
    lr_inputs = Input((None, None, num_images))
    
    # include hrnet pre-processing?
    n = lr_inputs
    # n = Lambda(lambda x: tf.pad(x, ((0, 0), (4, 4), (4, 4), (0, 0)), 'REFLECT'))(lr_inputs) # added
    if num_images > 1:
        n = SeparableConv2D(filters=64,
                            kernel_size=(9, 9),
                            strides=(1, 1),
                            depth_multiplier=25,
                            padding='same',
                            activation='linear',
                            kernel_initializer=RandomNormal(0., 0.02))(n) # modified
    else:
        n = Conv2D(filters=64,
                   kernel_size=(9, 9),
                   strides=(1, 1),
                   padding='same',
                   activation='linear',
                   kernel_initializer=RandomNormal(0., 0.02))(n)
                   
    n = PReLU(alpha_initializer=Constant(0.25), shared_axes=(1, 2))(n)
    
    skip_connect = n
    
    for _ in range(5): n = srgan_res_block(n)
    
    n = srgan_conv_bn(n)
    n = Add()([skip_connect, n])
    
    for _ in range(2): n = srgan_upsampling(n)
    
    # n = Lambda(lambda x: tf.pad(x, ((0, 0), (4, 4), (4, 4), (0, 0)), 'REFLECT'))(n) # added
    
    out = Conv2D(filters=1,
                 kernel_size=(9, 9),
                 strides=(1, 1),
                 padding='same',
                 activation='tanh',
                 kernel_initializer=RandomNormal(0., 0.02))(n) # modified
    
    '''
    out = SeparableConv2D(filters=1,
                          kernel_size=(9, 9),
                          strides=(1, 1),
                          depth_multiplier=4,
                          padding='same',
                          activation='tanh',
                          kernel_initializer=RandomNormal(0., 0.02))(n) # modified
    '''
    model = Model(lr_inputs, out)
    model.compile(optimizer=optimizer, loss=loss)
    
    return model
    

def srgan_conv_bn_leaky(_nn, filters, kernel, strides):
    _nn = Conv2D(filters=filters,
                 kernel_size=kernel,
                 strides=strides,
                 padding='same',
                 activation='linear',
                 kernel_initializer=RandomNormal(0., 0.02))(_nn)
    _nn = BatchNormalization(gamma_initializer=RandomNormal(1., 0.02))(_nn)
    _nn = LeakyReLU(0.2)(_nn)
    return _nn
    
    
def srgan_discriminator_model(input_shape, optimizer='adam', loss='mse'):
    unknown_inputs = Input(input_shape)
    n = unknown_inputs
    
    n = Conv2D(filters=64, kernel_size=(4, 4), strides=(2, 2), padding='same', activation='linear', kernel_initializer=RandomNormal(0., 0.02))(n)
    n = LeakyReLU(0.2)(n)
    
    n = srgan_conv_bn_leaky(n, 128, 4, 2)
    n = srgan_conv_bn_leaky(n, 256, 4, 2)
    n = srgan_conv_bn_leaky(n, 512, 4, 2)
    n = srgan_conv_bn_leaky(n, 1024, 4, 2)
    n = srgan_conv_bn_leaky(n, 2048, 4, 2)
    n = srgan_conv_bn_leaky(n, 1024, 1, 1)
    nn = srgan_conv_bn_leaky(n, 512, 1, 1)
    
    n = srgan_conv_bn_leaky(nn, 128, 1, 1)
    n = srgan_conv_bn_leaky(n, 128, 3, 1)
    n = Conv2D(filters=512, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear', kernel_initializer=RandomNormal(0., 0.02))(n)
    n = BatchNormalization(gamma_initializer=RandomNormal(1., 0.02))(n)
    
    n = Add()([nn, n])
    n = LeakyReLU(0.2)(n)
    
    n = Flatten()(n)
    n = Dense(1, activation='sigmoid')(n)
    
    model = Model(unknown_inputs, n)
    model.compile(optimizer=optimizer, loss=loss)
    
    return model
    

def denoise_model(num_input_channels=1, optimizer='adam', loss='mse'):
    sr_noised = Input((None, None, num_input_channels))
    n_skip = srgan_conv_bn_leaky(sr_noised, 64, 9, 1)
    
    n = srgan_conv_bn_leaky(n_skip, 64, 4, 2)
    n = srgan_conv_bn_leaky(n, 64, 4, 2)
    
    for _ in range(8): n = srgan_res_block(n)
    for _ in range(2): n = srgan_upsampling(n)
    
    n = Add()([n_skip, n])
    n = Conv2D(filters=1,
               kernel_size=(5, 5),
               strides=(1, 1),
               padding='same',
               activation='linear',
               kernel_initializer=RandomNormal(0., 0.02))(n)
    n = BatchNormalization(gamma_initializer=RandomNormal(1., 0.02))(n)
    n = PReLU(alpha_initializer=Constant(0.25), shared_axes=(1, 2))(n)
    n = Add()([sr_noised, n])
    
    model = Model(sr_noised, n)
    model.compile(optimizer=optimizer, loss=loss)
    
    return model
    
    

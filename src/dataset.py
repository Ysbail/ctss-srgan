import tensorflow as tf
import numpy as np
import pandas as pd
import netCDF4 as nc

# import src.ncHelper as ncHelper
import src.grid_tricks as grid_tricks

from ncBuilder import ncHelper
from tqdm import tqdm


class Dataset:
    def __init__(self, _input_path, _output_path, _dates, **kwargs):
        if isinstance(_input_path, str):
            _input_path = [_input_path]
        if isinstance(_output_path, str):
            _output_path = [_output_path]
            
        self.input_path = _input_path
        self.output_path = _output_path
        self.dates = np.asanyarray(_dates)
        
        self.x_train = None
        self.y_train = None
        self.x_test = None
        self.y_test = None
        
        self._range_lats = kwargs.get('_range_lats', np.array([-90., 90.]))
        self._range_lons = kwargs.get('_range_lons', np.array([-180., 180.]))
        
        # self.methods = ['bilinear', 'lanczos3', 'lanczos5', 'bicubic', 'gaussian', 'nearest', 'area', 'mitchellcubic']
        self.methods = ['lanczos3', ]
        self.x_scaler = None
        self.y_scaler = None
        
        # default for cfs x merge
        # obsolete
        self.in_slice = slice(None) # kwargs.get('in_slice', slice(None, 4))
        self.out_slice = slice(None) # kwargs.get('out_slice', slice(None, 24, 6))
        
        self.in_var = kwargs.get('in_var', 'precipitation')
        self.out_var = kwargs.get('out_var', 'precipitation')
        # for merge x merge parse slice(None) for both
        
        self.in_filter = kwargs.get('in_filter', lambda x: np.clip(x, 0., 100.)) # Callable
        self.out_filter = kwargs.get('out_filter', lambda x: np.clip(x, 0., 100.)) # Callable
        
        self.in_target_dim = kwargs.get('in_target_dim', (32, 32))
        self.out_target_dim = kwargs.get('out_target_dim', (128, 128))
        
        self.in_agg = kwargs.get('in_agg', False)
        self.out_agg = kwargs.get('out_agg', False)
        
        self.input_crop = None
        self.output_crop = None
        
        self.idx_map = None
        
        self._eval()


    def _eval(self, ):
        _date = self.dates[0]
        
        # evaluate the coordinates
        _input_lats, _input_lons, _output_lats, _output_lons = self.load_latlons(_date)
        
        self._range_lats[0] = max([_input_lats.min(), _output_lats.min(), self._range_lats[0]])
        self._range_lats[1] = min([_input_lats.max(), _output_lats.max(), self._range_lats[1]])
        self._range_lons[0] = max([_input_lons.min(), _output_lons.min(), self._range_lons[0]])
        self._range_lons[1] = min([_input_lons.max(), _output_lons.max(), self._range_lons[1]])
                
        _lat_step = np.diff(_output_lats).mean().round(2)
        _lon_step = np.diff(_output_lons).mean().round(2)
        
        print('steps:', _lat_step, _lon_step)
        
        self.general_lats = np.arange(self._range_lats[0], self._range_lats[1] + _lat_step / 2, _lat_step)
        self.general_lons = np.arange(self._range_lons[0], self._range_lons[1] + _lon_step / 2, _lon_step)
        
        _lat_slice = slice(None, self.general_lats.size // self.out_target_dim[0] * self.out_target_dim[0])
        _lon_slice = slice(None, self.general_lons.size // self.out_target_dim[1] * self.out_target_dim[1])
        
        self.general_lats = self.general_lats[_lat_slice]
        self.general_lons = self.general_lons[_lon_slice]
        
        print('generals:', self.general_lats.shape, self.general_lons.shape)
        
        self.output_crop = (self.out_slice,
                            slice(np.argmin(abs(_output_lats - self.general_lats[0])), np.argmin(abs(_output_lats - self.general_lats[0])) + self.general_lats.size),
                            slice(np.argmin(abs(_output_lons - self.general_lons[0])), np.argmin(abs(_output_lons - self.general_lons[0])) + self.general_lons.size))
        self.input_crop = (self.in_slice,
                           slice(np.argmin(abs(_input_lats - self.general_lats[0])), np.argmin(abs(_input_lats - self.general_lats[0])) + self.general_lats.size),
                           slice(np.argmin(abs(_input_lons - self.general_lons[0])), np.argmin(abs(_input_lons - self.general_lons[0])) + self.general_lons.size))
                           
        print('crops:', self.input_crop, self.output_crop)
        self.input_resize = lambda x, method: tf.image.resize(x, [self.general_lats.size // 4, self.general_lons.size // 4], method).numpy()


    def _load_idx_map(self, _date):
        _in_nc_files = [nc.Dataset(path.format(date=_date), 'r', keepweakref=True) for path in self.input_path]
        _out_nc_files = [nc.Dataset(path.format(date=_date), 'r', keepweakref=True) for path in self.output_path]
        _nc_files = [*_in_nc_files, *_out_nc_files]
        
        _times = [pd.to_datetime(ncHelper.load_time(_nc_file['time'])) for _nc_file in _nc_files]
        _dfs = [pd.DataFrame(np.arange(_time.size), index=pd.Index(_time)) for _time in _times]
        self.idx_map = pd.concat(_dfs, axis=1, join='inner')

        [_nc_file.close() for _nc_file in _nc_files]


    @staticmethod
    def _handle_multiple_paths(paths, _var, _crop, _idx_map, _head_time_idx=0, _agg=False, **kwargs):
        _nc_files = [nc.Dataset(path, 'r', keepweakref=True) for path in paths]
        
        _array = []
        for idx, _nc_file in enumerate(_nc_files):
            _valid_idx = _idx_map.iloc[:, idx + _head_time_idx]
            if _agg: # gambi para resolver problema de agregação de variáveis (soma de precipitação)
                _sub_arr = []
                for _idx in _valid_idx:
                    _sub_arr.append(_nc_file[_var][_idx - kwargs.get('agg_range', 6): _idx + 1].sum(axis=0, keepdims=True)[_crop][..., None]) # adds a channel dimension
                _arr = np.concatenate(_sub_arr, axis=0)
            else:
                _arr = _nc_file[_var][_valid_idx][_crop][..., None] # adds a channel dimension
            _array.append(_arr)
            
        _array = np.concatenate(_array, axis=-1)
        
        [_nc_file.close() for _nc_file in _nc_files]
        
        return _array


    def load_data(self, _date):
        self._load_idx_map(_date)
        
        _array_input = self._handle_multiple_paths([_path.format(date=_date) for _path in self.input_path], self.in_var, self.input_crop, self.idx_map, _agg=self.in_agg)
        _array_output = self._handle_multiple_paths([_path.format(date=_date) for _path in self.output_path], self.out_var, self.output_crop, self.idx_map, len(self.input_path), _agg=self.out_agg)

        return _array_input, _array_output


    def load_latlons(self, _date):
        _input_lats, _input_lons = ncHelper.get_lats_lons(self.input_path[0].format(date=_date), 1, 0)
        _output_lats, _output_lons = ncHelper.get_lats_lons(self.output_path[0].format(date=_date), 1, 0)
        
        _input_lons[_input_lons > 180] -= 360
        _output_lons[_output_lons > 180] -= 360
        
        return _input_lats, _input_lons, _output_lats, _output_lons


    def process_data(self, _date):
        _x, _y = self.load_data(_date)
        _x = np.concatenate([self.input_resize(_x, method) for method in self.methods], axis=-1)
        
        _x = self.in_filter(_x)
        _y = self.out_filter(_y)
        
        # get chunks
        _x = grid_tricks.get_chunks(_x, target_dim=self.in_target_dim)
        _y = grid_tricks.get_chunks(_y, target_dim=self.out_target_dim)
        
        _x = _x.reshape((-1, *_x.shape[-3:]))
        _y = _y.reshape((-1, *_y.shape[-3:]))
        
        return _x, _y


    def _prepare_curr_dataset(self, ):
        self.x_train = []
        self.y_train = []
        
        for _date in tqdm(self.dates):
            _x_temp, _y_temp = self.process_data(_date)
            self.x_train.append(_x_temp)
            self.y_train.append(_y_temp)
            
        self.x_train = np.concatenate(self.x_train, axis=0)
        self.y_train = np.concatenate(self.y_train, axis=0)
        
        print(self.x_train.shape)
        print(self.y_train.shape)
        
        self.x_scaler = MinMaxSpatial((-1., 1.))
        self.y_scaler = MinMaxSpatial((-1., 1.))
        
        self.x_scaler.fit(self.x_train)
        self.y_scaler.fit(self.y_train)
        
        self.x_train = self.x_scaler.transform(self.x_train)
        self.y_train = self.y_scaler.transform(self.y_train)
        
        print('x_train shape', self.x_train.shape)
        print('y_train shape', self.y_train.shape)
        
        _test_size = 32
        self.x_test = self.x_train[-_test_size:]
        self.y_test = self.y_train[-_test_size:]
        
        self.x_train = self.x_train[:-_test_size]
        self.y_train = self.y_train[:-_test_size]
        
          
class MinMaxSpatial:
    def __init__(self, feature_range, clip=True):
        self.feature_range = feature_range
        self.clip = clip
        self._min = None
        self._data_min = None
        self._data_max = None
        self._data_range = None
        self._data_scale = None
        
        
    def fit(self, X):
        _shape = tuple(np.arange(np.ndim(X) - 1, dtype=np.uint8))
        self._data_min = np.min(X, axis=_shape, keepdims=True)
        self._data_max = np.max(X, axis=_shape, keepdims=True)
        self._data_range = self._data_max - self._data_min
        self._data_scale = (self.feature_range[1] - self.feature_range[0]) / self._data_range
        self._min = self.feature_range[0] - self._data_min * self._data_scale
        
        print('fitting MinMaxSpatial:', self._data_scale, self._min)
        
        
    def transform(self, X):
        X = X.copy()
        X *= self._data_scale
        X += self._min
        if self.clip:
            np.clip(X, self.feature_range[0], self.feature_range[1], out=X)
        return X
        
        
    def inverse_transform(self, X):
        X = X.copy()
        X -= self._min
        X /= self._data_scale
        return X
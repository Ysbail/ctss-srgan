import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import matplotlib
matplotlib.use('Agg')

import netCDF4 as nc
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.models import load_model
from src.dataset import MinMaxSpatial

import src.models as models
# import src.ncHelper as ncHelper

scaler_plot = MinMaxSpatial((-1, 1))
scaler = MinMaxSpatial((-1, 1))

nc_files = [nc.Dataset(r'D:\DATA\cfs\cfs_glo_total_precipitation_M00%d_2022012300.nc'%i) for i in range(4)]
grid = np.concatenate([nc_file['total_precipitation'][:][:1, ..., None] * 21600. for nc_file in nc_files], axis=-1)

print(grid.max(), grid.shape)

scaler_plot.fit(grid)
grid_plot = scaler_plot.transform(grid)

# lats, lons = ncHelper.get_lats_lons_file(nc_file, 1, 0)
# _target_shape = (int(np.diff(lats).mean() * lats.size) - 1, int(np.diff(lons).mean() * lons.size) - 1)

# methods = ['bilinear', 'lanczos3', 'lanczos5', 'bicubic', 'gaussian', 'nearest', 'area', 'mitchellcubic']
# grids = np.concatenate([tf.image.resize(grid, _target_shape, method).numpy() for method in methods], axis=-1)

# scaler.fit(grids)
# grids = scaler.transform(grids)

scaler.fit(grid)
grids = scaler.transform(grid)

# den = tf.keras.models.load_model('/work/home.operacao/operacao/francisco.viana/ctss/models/bkp_ctss_den_model.h5')
model = tf.keras.models.load_model('./models/bkp_ctss_gen_model.h5')
# model = models.srgan_generator_model(8)
# model.load_weights('/work/home.operacao/operacao/francisco.viana/ctss/models/bkp_ctss_gen_model.h5')

res = model.predict(grids)
# res_den = den.predict(res)

plt.figure(figsize=(19.2, 10.8), dpi=100)
plt.imshow(np.mean(grid_plot[0, ...], axis=-1), vmin=-1, vmax=1)
plt.savefig('./imgs/test_v2.png')
plt.close()

plt.figure(figsize=(19.2, 10.8), dpi=400)
plt.imshow(res[0, ..., 0], vmin=-1, vmax=1)
plt.savefig('./imgs/test_up_v2.png')
plt.close()

# plt.figure(figsize=(19.2, 10.8), dpi=400)
# plt.imshow(res_den[0, ..., 0])
# plt.savefig('./imgs/test_up_den_v2.png')
# plt.close()
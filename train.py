import tensorflow as tf
devices = tf.config.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(devices[0], True)

import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import pandas as pd
import numpy as np

from tqdm import tqdm

import src.models as models
from src.dataset import Dataset
from src.trainer import Trainer

# input_path = '/storagefinep/hrnet/cfs/dataset/cfs_glo_total_precipitation_M000_{date:%Y%m%d}00.nc'
# output_path = '/storagefinep/hrnet/merge/dataset/{date:%Y%m%d}_merge_dataset.nc'
# cfs_path = '/storagefinep/hrnet/cfs/dataset/cfs_glo_total_precipitation_M000_{date:%Y%m%d}00.nc'
# merge_path = '/storagefinep/hrnet/merge/dataset/{date:%Y%m%d}_merge_dataset.nc'

cfs_path_single = r'C:\Users\Eksoss\work\HRNet\cfs\dataset\cfs_glo_total_precipitation_M000_{date:%Y%m%d}00.nc'
cfs_path = [r'D:\DATA\cfs\cfs_glo_total_precipitation_M000_{date:%Y%m%d}00.nc',
            r'D:\DATA\cfs\cfs_glo_total_precipitation_M001_{date:%Y%m%d}00.nc',
            r'D:\DATA\cfs\cfs_glo_total_precipitation_M002_{date:%Y%m%d}00.nc',
            r'D:\DATA\cfs\cfs_glo_total_precipitation_M003_{date:%Y%m%d}00.nc']

merge_path = r'C:\Users\Eksoss\work\HRNet\merge\dataset\{date:%Y%m%d}_merge_dataset.nc'

# initial_date = '2022-01-23'
initial_date = '2022-01-01'
# final_date = '2022-01-26'
final_date = '2022-02-01'
epochs = 1000

dates = pd.date_range(initial_date, final_date, freq='D').to_pydatetime()
dates2 = pd.date_range('2022-01-23', '2022-01-26', freq='D').to_pydatetime()

# dataset = Dataset(input_path, output_path, dates) # regular usage

dataset = Dataset([merge_path] * 4, merge_path, dates) # using output data same as input data

dataset2 = Dataset(cfs_path, cfs_path, dates2, in_filter=lambda x: np.clip(x * 21600., 0., 100.), out_filter=lambda x: np.clip(np.mean(x, axis=-1, keepdims=True) * 21600., 0., 100.),
                                               in_var='total_precipitation',                      out_var='total_precipitation', 
                                               in_target_dim=(32, 32),                            out_target_dim=(128, 128))
                   # _range_lats=dataset._range_lats, _range_lons=dataset._range_lons)

'''
dataset = Dataset(cfs_path, merge_path, dates2, in_filter=lambda x: np.clip(x * 21600., 0., 100.), in_var='total_precipitation',
                                                out_filter=lambda x: np.clip(x, 0., 100.), out_agg=True)
'''
# print(dataset.general_lats.size, dataset.general_lons.size)
# print(dataset2.general_lats.size, dataset2.general_lons.size)
                   
dataset._prepare_curr_dataset()
dataset2._prepare_curr_dataset()

tf.keras.backend.clear_session()

num_images = dataset.x_train.shape[-1]
gen_model = models.srgan_generator_model(num_images)
dis_model = models.srgan_discriminator_model(dataset.y_train.shape[1:]) # (128, 128, 1)
den_model = models.denoise_model()

trainer = Trainer(gen_model, dis_model, den_model)

# trainer.use_pre_trained_weights('gen_model', './models/bkp_ctss_gen_model.h5') # internal attr name of the model
# trainer.use_pre_trained_weights('dis_model', './models/bkp_ctss_dis_model.h5')

# inverse_x_train = -1. * dataset.x_train
# inverse_y_train = -1. * dataset.y_train

# inverse2_x_train = -1. * dataset2.x_train
# inverse2_y_train = -1. * dataset2.y_train

batch_size = 16
_train_slices = [slice(i * batch_size, (i + 1) * batch_size) for i in range(dataset.y_train.shape[0] // batch_size)]

# increment dataset


for i in tqdm(range(epochs)):
    
    for batch in _train_slices:
        # trains over merge dataset
        # trainer.train_step(dataset.x_train[batch], dataset.y_train[batch])
        
        # trains over cfs dataset, ignores hr for training the discriminator model
        _sample = np.random.choice(np.arange(dataset2.x_train.shape[0]), size=batch_size)
        trainer.train_step_ignore_hr(dataset2.x_train[_sample], dataset2.y_train[_sample])
        
        # trainer.train_step_ignore_hr(inverse_x_train[batch], inverse_y_train[batch])
        # trainer.train_step_ignore_hr(inverse2_x_train[_sample], inverse2_y_train[_sample])
        
        # train again over merge
        trainer.train_step(dataset.x_train[batch], dataset.y_train[batch])
    
    # save every epoch, it's little time consuming when the dataset is huge
    trainer.gen_model.save('./models/bkp_ctss_gen_model.h5')
    trainer.dis_model.save('./models/bkp_ctss_dis_model.h5')
    # trainer.den_model.save('./models/bkp_ctss_den_model.h5')
    
    if (i + 1) % 5 == 0:
        print(i + 1, trainer.gen_tracker.result(), trainer.dis_tracker.result(), trainer.den_tracker.result())
        
        trainer.plot(dataset, i)
        trainer.plot(dataset2, i, 'cfs')

trainer.gen_model.save('./models/ctss_gen_model_v2.h5')
trainer.dis_model.save('./models/ctss_dis_model_v2.h5')
# trainer.den_model.save('./models/ctss_den_model_v2.h5')

# TODO
# trainer.plot()
